#include <vector>

class shape {
public:
  enum type {
    rect, circle
  };

  shape(type type) : type_(type) {}

  type type_;
};

class rect : public shape {
public:
  rect(int x, int y, int width, int height)
      : shape(shape::rect),
        x_(x), y_(y), width_(width), height_(height) {}

private:
  int x_, y_;
  int width_, height_;
};

class circle : public shape {
public:
  circle(int x, int y, int radius)
      : shape(shape::circle),
        x_(x), y_(y), radius_(radius) {}

private:
  int x_, y_;
  int radius_;
};

class shape_printer {
public:
  void print(shape const &s) {
    switch (s.type_) {
      case shape::rect:
        // ...
        break;

      case shape::circle:
        // ...
        break;
    }
  }
};

int main() {
  std::vector<shape *> shapes;

  shapes.push_back(new rect{1, 2, 3, 4});
  shapes.push_back(new circle{2, 2, 42});

  shape_printer printer;
  for (auto shape : shapes) {
    printer.print(*shape);
  }
}
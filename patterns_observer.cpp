#include <string>
#include <vector>
class NewsItem
{
public:
  std::string title;
};

class NewsObserver
{
public:
  virtual void on_news_received(NewsItem const& news) = 0;
};

class NewsSource
{
public:
  void crawl()
  {
    // ...
    NewsItem item{"Test"};
    for (auto &observer : observers_)
      observer.on_news_received(item);
  }

private:
  std::vector<NewsObserver> observers_;
};

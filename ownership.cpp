#include <iostream>
#include <vector>

class Room
{
public:
  Room(Room &prev, Room &next)
    : prev_{prev},
      next_{next}
  {}

private:
  Room &prev_;
  Room &next_;
};

class Map
{
public:
  Map()
  {
    // create rooms...
  }

private:
  std::vector<Room> rooms_;
};

class Game
{
public:

private:
  Map map_;
};

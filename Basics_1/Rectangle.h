#ifndef BASICS_1_RECTANGLE_H
#define BASICS_1_RECTANGLE_H

/*
 * Convention:
 * One class declaration per .h file
 * All definitions go into the corresponding .cpp file
 */
class Rectangle
{
    /*
     * "private" members of a class can only be accessed by objects of the class itself
     * private functions cannot be called from outside
     * private variables can neither be read nor written directly from outside
     */
private:
    /*
     * In this implementation, these variables must not be set to negative values.
     * However, use of "unsigned" data types is discouraged by the C++ standard due to potential problems arising from
     * implicit conversions from signed to unsigned
     *
     * Both variables are initialized to 1 (for every object at creation), so this way there's no need to set them in
     * the constructor
     *
     * Convention:
     * Add a "_" at the end of member variables with setters to differentiate them from function parameters of the same name
     */
    int height_ = 1;
    int width_ = 1;


    
    /*
     * "public" members of a class can be accessed by everyone with access to an object of the class
     * This means that public variables can potentially be set to any value, at any time, form outside.
     * This is usually a bad idea!
     */
public:
     /*
      * The constructor is used to set up an object of the class for proper operation upon the object's creation
      * The default (no arguments) constructor uses the values given above for the member variables.
      * When declared " = default", there's no need to implement an empty body
      */
    Rectangle() = default;
    // A class can have several different constructors if they specify different arguments.
    // If an argument is defined " = $VALUE$", it'll use that as default value if the argument isn't given upon call
    Rectangle(int height = 1, int width = 1);
    
    /*
     * To access private variables from outside, getter and setter functions are implemented.
     * These can assure proper operation when called rather then giving a non-member free hand.
     */
    void set_height(int height);
    void set_width(int width);
    
    /*
     * Convention:
     * For getter functions, it's good practice to return the value as const.
     * Otherwise a caller is able to use the variable's pointer to change the value from outside,
     * thereby bypassing encapsulation.
     */
    int get_height() const;
    int get_width() const;
    
};


#endif //BASICS_1_RECTANGLE_H

#include "Rectangle.h"

#include <iostream>



/*
 * By standard function arguments are passed "by value". The variable passed as argument is copied to the function.
 * For literal types, this is usually of no concern. When passing objects, depending on the class, it can mean a 
 * significant workload.
 * The "&" operator here declares the parameter as a reference to the value. References are similar to pointers but 
 * with some caveats. They point directly the value of the assigned variable.
 * As a function parameter, this means arguments are not copied but given directly. For literals this is trivial and 
 * rarely has any benefits.
 * If the argument is an object, it forgoes a potentially arduous copy instantiation process.
 * Furthermore, it allows the function to work directly on (i.e. change) the argument itself.
 * 
 * References can never be NULL and must be initialized upon creation. Members are addressed by "." rather than "->"
 * 
 * We define the parameter here as const since our function here doesn't intend to (and therefore should not be allowed 
 * to by design) change anything in the passed argument.
 * 
 * Note:
 * In the next example this function will be refactored into several different classes. This will show variations of 
 * usage within those classes. The code for the function itself, however, will remain the same from here on out.
 */
void display(const Rectangle &rect)
{
    std::cout << std::endl;
    for (int row = 1 ; row <= rect.get_height() ; row++)
    {
        for (int column = 1 ; column <= rect.get_width() ; column++)
        {
            std::cout << "#";
            // add a space after each symbol if another one follows for formatting
            if (column < rect.get_width())
                std::cout << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}






/*
 * Goal:
 * We want to display rectangle shapes in the console with the symbol '#'.
 * Rectangles can have varying lengths for their height and width
 */
int main()
{
    /*
     * Convention:
     * Constructors should be called with curly brackets to avoid compilers confusing them with function declarations
     */
    Rectangle rect{3, 8};
    
    display(rect);
    
    rect.set_width(5);
    rect.set_height(4);
    
    display(rect);
    
    rect.set_height(-8);
    
    display(rect);
    
    return 0;
}





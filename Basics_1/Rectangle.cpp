#include "Rectangle.h"


int Rectangle::get_height() const
{
    return height_;
}


void Rectangle::set_height(int height)
{
    if (0 < height)
        height_ = height;
}


int Rectangle::get_width() const
{
    return width_;
}


void Rectangle::set_width(int width)
{
    if (0 < width)
        width_ = width;
}

/*
 * The constructor utilizes the setter functions to ensure proper values were given as arguments.
 * The way we set up the constructor in the .h file ensures that if a constructor argument is invalid,
 * the default value (1 in this case) is used instead.
 */
Rectangle::Rectangle(int height, int width)
{
    set_height(height);
    set_width(width);
}

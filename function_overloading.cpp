class point {
public:
  point(int x, int y) : x_(x), y_(y) {}

private:
  int x_, y_;
};

class rectangle {
public:
  rectangle(point top_left, point bottom_right);
  rectangle(point top_left, int height, int width);
};

int main()
{
  point top_left{1, 2};

  point bottom_right{3, 4};
  rectangle r1{top_left, bottom_right};

  rectangle r2{top_left, 2, 2};
}
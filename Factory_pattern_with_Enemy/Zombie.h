#ifndef FACTORYPATTERN_01_ZOMBIE_H
#define FACTORYPATTERN_01_ZOMBIE_H

#include "Enemy.h"

class Zombie : public Enemy {

protected:
    void resurrect ();
    
public:
    Zombie();
    void attack() override;
    void die() override;
    std::string get_enemy_definition() const override;
    int get_default_max_health() override;

};


#endif //FACTORYPATTERN_01_ZOMBIE_H

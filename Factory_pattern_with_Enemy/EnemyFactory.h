#ifndef FACTORYPATTERN_01_ENEMYFACTORY_H
#define FACTORYPATTERN_01_ENEMYFACTORY_H

#include "Enemy.h"
#include "Goblin.h"
#include "Zombie.h"
#include <memory>


enum class EnemyType
{
    Goblin, Zombie
};


/*
 * Static factory pattern for Enemy objects
 */
class EnemyFactory {
public:
    static std::unique_ptr<Enemy> create_enemy(EnemyType type);

};


#endif //FACTORYPATTERN_01_ENEMYFACTORY_H

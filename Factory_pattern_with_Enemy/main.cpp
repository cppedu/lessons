#include <iostream>
#include <memory>

#include "EnemyFactory.h"

int main() {
    
    std::unique_ptr<Enemy> ein_goblin = EnemyFactory::create_enemy(EnemyType::Goblin);
    std::unique_ptr<Enemy> ein_zombie = EnemyFactory::create_enemy(EnemyType::Zombie);
    
    ein_goblin->attack();
    ein_zombie->attack();
    
    // basic damage
    ein_goblin->receive_damage(9);
    ein_zombie->receive_damage(24);
    
    // additional damage enough to kill enemies
    // "overkill" on goblin
    ein_goblin->receive_damage(8);
    ein_zombie->receive_damage(1);
    


    return 0;
}
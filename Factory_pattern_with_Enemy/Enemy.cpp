#include "Enemy.h"
#include <iostream>


Enemy::Enemy (int max_health)
{
    current_health_ = max_health_ = max_health;
}


/*
 * Enemy takes the given damage flat to his health and dies if health reaches 0
 * Health can never reach negative values
 */
void Enemy::receive_damage(int damage) {
    
    std::cout << get_enemy_definition() << " was hit with " << damage << " damage" << std::endl;
    
    if (current_health_ - damage <= 0)
    {
        current_health_ = 0;
        die();
        return;
    } else
    {
        current_health_ -= damage;
        std::cout << get_enemy_definition() << " health: " << current_health_ << "/" << max_health_ << std::endl;
        return;
    }
}



void Enemy::die()
{
    std::cout << get_enemy_definition() << " has died" << std::endl;
}






#include "Goblin.h"
#include<iostream>

Goblin::Goblin() : Enemy(get_default_max_health())
{
    std::cout << "A " << get_enemy_definition() << " crawls from his hole" << std::endl;
}


void Goblin::attack()
{
    std::cout << "Goblin attacks" << std::endl;
}


std::string Goblin::get_enemy_definition() const
{
    return "Goblin";
}



int Goblin::get_default_max_health()
{
    return 12;
}

#ifndef FACTORYPATTERN_01_GOBLIN_H
#define FACTORYPATTERN_01_GOBLIN_H

#include "Enemy.h"

class Goblin : public Enemy
{
public:
    Goblin();
    void attack() override;
    std::string get_enemy_definition() const override;
    int get_default_max_health() override;
};


#endif //FACTORYPATTERN_01_GOBLIN_H

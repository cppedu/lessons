#ifndef FACTORYPATTERN_01_ENEMY_H
#define FACTORYPATTERN_01_ENEMY_H

#include <string>


class Enemy {

protected:
    int max_health_;
    int current_health_;
    virtual void die();

public:
    Enemy(int max_health);
    virtual int get_default_max_health() = 0;
    virtual std::string get_enemy_definition() const = 0 ;
    virtual void receive_damage(int damage);
    virtual void attack() = 0;
};


#endif //FACTORYPATTERN_01_ENEMY_H

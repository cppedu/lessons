#include "EnemyFactory.h"


std::unique_ptr<Enemy> EnemyFactory::create_enemy(EnemyType type)
{
    switch(type)
    {
        case EnemyType::Goblin:
            return std::make_unique<class::Goblin>();
        case EnemyType::Zombie:
            return std::make_unique<class::Zombie>();
    }
}







#include "Zombie.h"
#include<iostream>


Zombie::Zombie() : Enemy(get_default_max_health())
{
    std::cout << "A " << get_enemy_definition() << " rises from the grave" << std::endl;
}


void Zombie::attack()
{
    std::cout << "Zombie attacks" << std::endl;
}


/*
 * A Zombie never dies completely, he resurrects with full health
 */
void Zombie::resurrect()
{
    current_health_ = max_health_;
    std::cout << "Zombie resurrects" << std::endl;
}


/*
 * When a Zombie dies, he does so like any other Enemy but immediately resurrects
 */
void Zombie::die()
{
    Enemy::die();
    resurrect();
}

std::string Zombie::get_enemy_definition() const
{
    return "Zombie";
}


int Zombie::get_default_max_health()
{
    return 25;
}

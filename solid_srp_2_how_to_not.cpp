#include <vector>

class shape {
public:
  virtual void draw() = 0;
};

class rect : public shape {
public:
  rect(int x, int y, int width, int height)
      : x_(x), y_(y), width_(width), height_(height) {}

  void draw() override {
    // ...
  }

private:
  int x_, y_;
  int width_, height_;
};

class circle : public shape {
public:
  circle(int x, int y, int radius)
      : x_(x), y_(y), radius_(radius) {}

  void draw() override {
    // ...
  }

private:
  int x_, y_;
  int radius_;
};

int main()
{
  std::vector<shape*> shapes;

  shapes.push_back(new rect{1, 2, 3, 4});
  shapes.push_back(new circle{2, 2, 42});

  // shape *shapes[2] = {
  //     new rect{1, 2, 3, 4},
  //    new circle{2, 2, 42}
  // };

  for (auto shape : shapes) {
    shape->draw();
  }
}
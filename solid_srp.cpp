#include <ostream>
#include <iostream>
class point {
public:
  point(int x, int y) : x_(x), y_(y) {}

private:
  int x_, y_;
};

class rectangle {
public:
  rectangle(point const &top_left, point const &bottom_right)
      : top_left_(top_left), bottom_right_(bottom_right) {}
public:

private:
  point top_left_;
  point bottom_right_;
};

class ostream_printer {
public:
  ostream_printer(std::ostream &out) : out_(out) {}

  void print(rectangle const &rect);

private:
  std::ostream &out_;
};

int main() {
  ostream_printer printer{std::cout};

  rectangle rect({1, 2}, {3, 4});
  printer.print(rect);
}
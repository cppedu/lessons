#include <iostream>

class Point {
public:
  Point(int x, int y) : x_(x), y_(y) {}

  int get_x() const {
    return x_;
  }

  int get_y() const {
    return y_;
  }

private:
  int x_, y_;
};

std::ostream &operator<<(std::ostream &out, Point const &point) {
  return out << "(" << point.get_x() << "|" << point.get_y() << ")";
}

int main() {
  Point point{1, 2};

  // Das:
  std::cout << "(" << point.get_x() << "|" << point.get_y() << ")\n";

  // vs.

  // das:
  std::cout << point << "\n";
}
#include <variant>

class rect
{

};

class circle
{

};

using shape = std::variant<rect, circle>;

class shape_printer
{
public:
  void operator()(rect const& r);
  void operator()(circle const& c);
};

void draw(shape const& s)
{
  std::visit(shape_printer{}, s);
}